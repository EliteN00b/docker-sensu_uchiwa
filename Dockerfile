FROM ubuntu:14.04

ENV DEBIAN_FRONTEND="noninteractive"

RUN \
    apt-get -qq update && \
    apt-get -yqq install \
        wget heirloom-mailx
RUN \
    wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && \
    dpkg -i erlang-solutions_1.0_all.deb && \
    wget -q http://www.rabbitmq.com/rabbitmq-signing-key-public.asc -O- | apt-key add - && \
    echo "deb     http://www.rabbitmq.com/debian/ testing main" > /etc/apt/sources.list.d/rabbitmq.list && \
    wget -q http://repositories.sensuapp.org/apt/pubkey.gpg -O- | apt-key add - && \
    echo "deb     http://repositories.sensuapp.org/apt sensu main" > /etc/apt/sources.list.d/sensu.list
RUN \
    apt-get -qq update && \
    apt-get -yqq install \
        erlang rabbitmq-server redis-server sensu uchiwa

ADD sensu/ /etc/sensu/
RUN touch /etc/sensu/IAMHERE
RUN chown -R sensu:sensu /etc/sensu
RUN cp -R /etc/sensu /var/sensu-template

VOLUME /etc/sensu/

EXPOSE 3000 4567 5672

ADD boot.sh /boot.sh
RUN chmod +x /boot.sh

CMD ["/boot.sh"]
