#!/bin/bash

if [[ ! -f "/etc/sensu/IAMHERE" ]]; then
  cp -R /var/sensu-template/* /etc/sensu/
fi

/etc/init.d/rabbitmq-server start

if ! rabbitmqctl list_vhosts | grep -q "/sensu"; then
  rabbitmqctl add_vhost /sensu
  rabbitmqctl add_user sensu sensu
  rabbitmqctl set_permissions -p /sensu sensu ".*" ".*" ".*"
fi

/etc/init.d/redis-server start
/etc/init.d/sensu-server start
/etc/init.d/sensu-api start
/etc/init.d/uchiwa start

exec bash
