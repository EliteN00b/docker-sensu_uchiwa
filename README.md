## Build


## Start

Start with some ports `8082` for Uchiwa website, `6000` for the API and `6001` for rabbitmq.

`docker run -di --name sensu_uchiwa_test -p 8082:3000 -p 6000:4567 -p 6001:5672 sensu_uchiwa:1.0`

## Add a client

Prepare client:

1. Install sensu: `gem install sensu`
2. Install plugin to monitor disk usage: `sensu-install -p disk-checks`

Example for `/etc/sensu/client.json`
```
{
  "client": {
    "name": "HOSTNAME",
    "address": "HOST_IP",
    "subscriptions": [
      "test"
    ]
  },
  "rabbitmq": {
    "host": "localhost",
    "port": 6001,
    "vhost": "/sensu",
    "user": "sensu",
    "password": "sensu"
  }
}
```

Start sensu client: `sensu-client -c /etc/sensu/client.json`

## Service ports

| Port | Description    |
|------|----------------|
| 3000 | Uchiwa Website |
| 4567 | Uchiwa API     |
| 5672 | RabbitMQ       |


## More checks

Useful gems:

```bash
gem install sensu-plugins-memory-checks
gem install sensu-plugins-cpu-checks
```


## Better mail support

```
/opt/sensu/embedded/bin/gem install mail --no-ri --no-rdoc
curl -sL "https://raw.github.com/sensu/sensu-community-plugins/master/handlers/notification/mailer.json"
curl -sL "https://raw.github.com/sensu/sensu-community-plugins/master/handlers/notification/mailer.rb"
```
